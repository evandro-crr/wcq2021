---
title: 'Minicourses'
weight: 40
header_menu: true
---

-------------------------------

# Quantum Computing Introduction with Ket 

[*Evandro Chagas*](#evandro-chagas)

In this minicourse we will introduce you to the basics of quantum computing from scratch and test them in the quantum programming language [Ket](https://quantum-ket.gitlab.io). During the minicourse, we will use the quantum computing simulator KBW to look into quantum execution, something that is not possible using a quantum computer. We hope that all participants interact during the course, expressing their doubts and testing what they have learned. It is not necessary to have any previous knowledge in quantum computing to follow the minicourse. However, basics of linear algebra and Python programming will help you to accompany the minicourse. 

<details>
  <summary><b>» Necessary requirements to follow the minicourse </b></summary>
  
* Updated Linux operating system 
* Python 3.7 or higher
* [Ket](https://gitlab.com/quantum-ket/ket)
* [KBW](https://gitlab.com/quantum-ket/kbw)

To install the Ket and KBW just run:
```console
pip install ket-lang kbw
```
</details>

<details>
  <summary><b>» If you don't have Linux and/or are not familiar with the system</b></summary>
  
We recommend creating a virtual machine with [Ubuntu 20.04](https://ubuntu.com/download/desktop). After installing the operating system, you need to install the necessary tools. Open a terminal and paste the following commands lines: 

```console
sudo apt update -y
sudo apt install python3-pip -y 
pip3 install ket-lang kbw
```

To open the KBW server just run:
```console
python3 -m kbw
```

For the quantum programming during the minicourse, we recommend using Visual Studio Code. To install on Ubuntu just run the following command in the terminal: 
```console
sudo snap install code --classic
```
</details>

Presentation: [[pdf](../pdfs/ket.pdf)] \
QAOA: [[zip](../pdfs/qaoa_ket.zip)]

Part 1:
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/LnNh-l1_bNc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Part 2:
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/E44ejmGi7lg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

-------------------------------

# Introduction to Quantum Information Using Qiskit

[*Bruna Gabrielly de Moraes Araújo*](#bruna-gabrielly-de-moraes-araújo)

We're going to have a basic introduction to quantum information, quantum computing to the teleportation protocol and then we're going to apply these ideas using the Qiskit tools and running some experiments using a real quantum computer!

Prerequisites, recommendations and tools needed to complete the short course.

Anyone who doesn't know quantum mechanics or even programming might be able to follow the course.

It is recommended that everyone make an account at: https://quantum-computing.ibm.com 

Notebooks: [[zip](../pdfs/notebooks_qiskit.zip)]

Part 1:
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/udNkZowXQ9E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Part 2:
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/oMKxONptg2A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

-------------------------------

# What is wrong with current quantum computers and how can we improve them?

[*André Carvalho*](#andré-carvalho)

In this short course we'll discuss current limitations of quantum
computers, the challenges faced when running algorithms on real devices,
and how quantum control techniques can be used to improve hardware
performance. We will introduce you to Boulder Opal, Q-CTRL's product aimed
at characterizing, improving and automating quantum hardware. Starting from
an idealized model of a superconducting qubit hardware, you'll learn how to
add different layers of imperfections until achieving a reasonably
realistic model of the system. You'll be able to use this model to simulate
the application of quantum gates on the qubit and to analyze, with the help
of Boulder Opal's visualization tools, the effects of imperfections on the
final gate performance. We will then present different quantum control
strategies to mitigate errors in the system, their advantages and
disadvantages, and apply them to the realistic qubit model presented
previously.

Parte 1:
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2dJ31N7gq48" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Parte 2:
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/3fOnBJcXUTQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
