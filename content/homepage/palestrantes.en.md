---
title: 'Speakers'
weight: 50
header_menu: false 
---

--------------------------------------

# András Gylien
*Caltech (EUA)*

![András Gylien](../fotos/andras.jpg)

András Gilyén is a Marie Curie fellow at the Rényi
Institute. His main research topic is quantum algorithms and complexity,
with a particular focus on quantum linear algebra methods (quantum singular
value transformation and the block-encoding framework), optimization, and
quantum walks. András received his PhD in 2019 from the University of
Amsterdam, where he was supervised by Ronald de Wolf and co-supervised by
Harry Buhrman at CWI/QuSoft. Between 2019 and 2021 he was an IQIM
postdoctoral fellow at Caltech, meanwhile he received the ERCIM Cor Baayen
Young Researcher Award in 2019 and was a Google Research Fellow at the
Simons Institute for the Theory of Computing in Berkeley during the "The
Quantum Wave in Computing" program in the spring of 2020.

--------------------------------------

# André Carvalho
*Q-CTRL (Austrália)*

![André Carvalho](../fotos/andre.jpg)

Andre Carvalho is the Head of Quantum Control Solutions at Q-CTRL.
He completed his PhD at the Federal University of Rio de Janeiro before
joining the Max-Planck Institute for the Physics of Complex Systems in
Dresden as a postdoctoral fellow. He then spent more than a decade as a
researcher at the Australian National University, where he worked across
disciplines, bringing together expertise from physicists and engineers to
develop control solutions for quantum technology problems. He has also
worked as a senior research fellow at Griffith University.
Andre has a vast international experience and is widely recognised for his
contributions to the field of quantum control.

--------------------------------------
# Ashley Montanaro
*University of Bristol (United Kingdom)*

![Ashley Montanaro](../fotos/montanaro.jpg)

Ashley Montanaro has worked in the field of quantum computing for 17 years, specialising in quantum algorithms and quantum computational complexity, and has published over 50 papers on this topic. He holds a PhD in quantum computing from the University of Bristol, supervised by Prof. Richard Jozsa, has been a postdoctoral fellow at the University of Cambridge, and is now Professor of Quantum Computation at Bristol. He holds an ERC Consolidator grant and was awarded a Whitehead Prize in 2017 by the London Mathematical Society. He served on the Steering Committee of the international conference on Quantum Information Processing (QIP) from 2016-19, and was a Founding Editor of the journal Quantum. He is co-founder of Phasecraft, a quantum software startup whose goal is to get the most out of near-term quantum computers.

--------------------------------------

# Bruna Gabrielly de Moraes Araújo
*The Institute of Photonic Sciences (Espanha)*

![Bruna Gabrielly de Moraes Araújo](../fotos/bruna.jpg)

Bruna has a Bachelor's Degree in Chemical Engineering from Unicap - PE, a Bachelor's Degree in Theoretical Physics at UFPE (Recife, Brazil) and at the University of Porto (Porto, Portugal). She also holds a Master in Physics where she worked with quantum logic theory in a single ion ion trap. She is actually a phd student in physics at ICFO,ICN2 &UAB. Thesis topic: Entanglement in many-body physics. She did an exchange at IBM Quantum from April to September 2021.

--------------------------------------

# Bruno Taketani
*IQM Quantum Computers (Germany)*

![Bruno Taketani](../fotos/bruno.png)

Bruno did his Ph.D. at the Federal University of Rio de Janeiro, working with quantum optics and quantum information/computation protocols. He then followed with two postdocs at the University of Saarland (Germany), developing protocols and algorithms for quantum computation with ions and superconducting qubits. In 2016, Bruno started a professorship at the Federal University of Santa Catarina in Brazil, where he helped strengthen the efforts on quantum computation. Since Sept. 2020 he has been working at IQM Germany, where he leads the Algorithm and Applications team, as well as the efforts to integrate IQM's quantum computers into HPC environments.

--------------------------------------

# Enrique Solano
*Kipu Quantum (Germany) \
University of the Basque Country (Spain) \
Shanghai University (China)*

![Enrique Solano](../fotos/enrique.jpg)

Prof. Enrique Solano got his Bachelor and Master in Physics at Pontificia
Universidad Católica del Perú, Lima, Perú. He obtained his PhD in Physics at Federal
University of Rio de Janeiro, Rio de Janeiro, Brazil. He realized postdoctoral research at
Max-Planck Institute for Quantum Optics, Garching, Germany, and Ludwig-Maximilian
University, Munich, Germany. In 2000, he was appointed Professor of Physics at Pontificia
Universidad Católica del Perú, Lima, Peru. In 2008, he accepted the prestigious position of
Ikerbasque Professor at University of the Basque Country, Bilbao, Spain, and was director
of the international center Quantum Computing and Quantum Technologies (QUTIS). In
2018, he accepted the position of Distinguished Professor of Physics at Shanghai
University, Shanghai, China, where he has been the director of the international center
&quot;Quantum Artificial Intelligence for Science and Technology (QuArtist)&quot;. In 2019, he was
appointed as Chief Scientific Officer of the AI company DeepBlue Technology based in
Shanghai, China. In 2020, Prof. Solano occupied the position of CEO of IQM Germany.
Since 2021, he is CEO of Kipu Quantum, boosting multiplatform modular codesign
quantum computers.

--------------------------------------

# Ernesto Galvão
*International Iberian Nanotechnology Laboratory (Portugal) \
Federal University Fluminense (Brazil)*

![Ernesto Galvão](../fotos/ernesto.jpg)

Ernesto F. Galvão is the leader of the Quantum and Linear-Optical Computation group at INL. He holds degrees in physics from the University of Oxford (PhD 2002), Federal University of Rio de Janeiro (Master´s 1998), and Pontifical Catholic University of Rio de Janeiro (Bachelor´s 1996). He was a postdoctoral fellow at Perimeter Institute for Theoretical Physics (Canada) between 2002 and 2005 and is currently on leave from the Instituto de Física of Fluminense Federal University (Brazil), where he has been faculty since 2006.
In his research he studies different quantum computational models to identify and quantify resources capable of achieving a quantum advantage in information processing. He is particularly interested in photonic implementations of quantum computers and collaborates with experimental quantum optics groups to implement and characterize these devices.

--------------------------------------

# Evandro Chagas
*Federal University of Santa Catarina (Brazil)*

![Evandro Chagas](../fotos/evandro.jpg)

Evandro is a Master's student in the Postgraduate Program in Computer Science at the Federal University of Santa Catarina, and has a degree in Computer Science from the same university. In his master's degree, he developed the classical-quantum programming language Ket, which focuses on quantum cloud computing and the interaction between classical and quantum information. His research area also covers quantum computation simulation, where he developed new techniques that allow reducing the simulation time of certain quantum algorithms. Evandro is a member of the UFSC Quantum Computing Group, where he participated in the organization of all editions of the Quantum Computing Workshop. 

--------------------------------------

# Marcos Villagra
*Universidad Nacional de Asunción (Paraguay)*

![Marcos Villagra](../fotos/marcos.png)

Marcos Villaga received a PhD degree in theoretical computer science from Nara Institute of Science and Technology, Japan (March 2013). He was a Research Fellow of the Japan Society for the Promotion of Sciences (JSPS) and a postdoctoral researcher at the University of Fukui from April 2013 to March 2015. Since April 2015 he is a full-time research professor at the National University of Asunción, Paraguay. His research interests are computational and algebraic complexity, cryptography, quantum computing and information, combinatorial games, automata theory, and algebraic and spectral algorithms.

--------------------------------------

# Walmoli Gerber Jr.
*QuanBy Quantum Computing (Brazil)*

![Walmoli Gerber Jr.](../fotos/walmoli.jpg)
