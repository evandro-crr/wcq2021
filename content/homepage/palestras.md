---
title: 'Palestras'
weight: 30
header_menu: true
---

-------------------------------

# Progressos em computação quântica fotônica
*(Palestra em ingles)*

[*Ernesto Galvão*](#ernesto-galvão)

Recentemente houve demonstrações impressionantes de computadores quânticos de pequena escala em várias plataformas físicas. Vou revisar alguns dos avanços recentes na área, com foco no uso de sistemas fotônicos para computação quântica, descrever dispositivos de amostragem de bósons, que são um tipo restrito de computador quântico fotônico e delinear caminhos possíveis para a computação quântica fotônica escalonável.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/9Q9WNnCtud8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Apresentação: [[pdf](pdfs/recent_progress_phot_comp_ufsc_iv_qc_workshop_slides.pdf)]

-------------------------------

# Perspectivas do mercado da computação quântica e impacto na competitividade global

[*Walmoli Gerber Jr.*](#walmoli-gerber-jr)

O papel do físico na sociedade vai muito além da academia. É dever do físico promover a sua inserção no mercado de trabalho tendo em vista a real possibilidade de empreendimento, principalmente no setor de tecnologia, uma vez que as ferramentas, conceitos e estratégias de raciocínio elaborados na graduação em física possibilitam o desenvolvimento tecnológico.
Existem inúmeras posições onde o conhecimento em física tem valor e grande potencial. Vamos abordar o cenário da computação quântica e demonstrar um caso brasileiro de empreendedorismo na física: a QuanBy é a primeira startup brasileira focada no desenvolvimento de tecnologias digitais em computação quântica, com o objetivo de fomentar o desenvolvimento do mercado de computação quântica no Brasil e oferecer tecnologias baseadas em computação quântica e comunicação.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Qw8dNohcL4k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

-------------------------------

# Co-projetando aceleradores quânticos específicos de aplicativos para HPC

[*Bruno Taketani*](#bruno-taketani)

IQM está se concentrando em uma abordagem de co-design, onde hardware e software são desenvolvidos lado a lado. A pilha de computação quântica específica do aplicativo resultante pode ser perfeitamente integrada em uma infraestrutura de HPC, de modo que o processador quântico seja utilizado em uma abordagem de acelerador. Para conseguir essa integração, a IQM também está construindo ferramentas de software e hardware específicas para HPC. Nesta palestra, apresentaremos esses conceitos e como a colaboração com especialistas no domínio nos levará adiante. Também exploraremos brevemente os possíveis caminhos para uma carreira na crescente indústria de computação quântica.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Y9v96MmwQPk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

-------------------------------

# Algoritmo Quântico para Otimização Multiobjetivo
*(Palestra em ingles)*

[*Marcos Villagra*](#marcos-villagra)

Nesta palestra irei discutir sobre construção de algoritmos quânticos para otimização multiobjetivo. Primeiro, apresentarei uma técnica heurística baseada no algoritmo de pesquisa de Grover. Em seguida, mostrarei como incorporar um problema multiobjetivo em um hamiltoniano e usar um algoritmo adiabático quântico para encontrar um estado fundamental que codifica uma solução  Pareto-ótima. Esta palestra será baseada nestes dois artigos: 1) **Multiobjective Optimization Grover Adaptive Search**. Benjamín Barán & Marcos Villagra. Recent Advances in Computational Optimization. Studies in Computational Intelligence, Vol.795, pp. 191-211, 2019, e 2) **A Quantum Adiabatic Algorithm for Multiobjective Combinatorial Optimization**. Benjamín Barán & Marcos Villagra. Axioms, Vol. 8, No. 1, March 2019.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/AAICWnPN4uo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

-------------------------------

# Algoritmos quânticos para resolver equações diferenciais
*(Palestra em ingles)*

[*Ashley Montanaro*](#ashley-montanaro)

Prevê-se que os computadores quânticos superem os clássicos na resolução de equações diferenciais, talvez exponencialmente. Mas será mesmo assim? Nesta palestra, vou descrever trabalhos recentes sobre algoritmos quânticos para resolver equações diferenciais de interesse para aplicações práticas. Primeiramente, consideramos uma equação diferencial parcial prototípica - a equação do calor em uma região retangular - e comparamos as complexidades de oito algoritmos clássicos e quânticos para resolvê-la, no sentido de calcular aproximadamente a densidade do calor em uma determinada região. Acontece que, para dimensão espacial d <= 3, os algoritmos quânticos são mais lentos que os melhores algoritmos clássicos. Para d >= 4, há uma aceleração polinomial no algoritmo quântico usando uma abordagem baseada na aplicação de estimativa de amplitude a um passeio aleatório. Em segundo lugar, discutirei um algoritmo quântico para resolver equações diferenciais de interesse em finanças matemáticas, como a equação de Black-Scholes, de forma mais eficiente do que os métodos clássicos. Este algoritmo é baseado em uma aceleração quântica do método de Monte Carlo multinível.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/6NTEpVrVbYw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

-------------------------------

# Computadores NISQ para Quantum Advantage (NISQA)
*(Palestra em ingles)*

[*Enrique Solano*](#enrique-solano)

Vou apresentar a computação quântica como um campo emergente proveniente da física quântica e da informação quântica no quadro da Física teórica e experimental, e como ela se moveu em direção a atividades empresariais em todo o mundo. Vou explicar como é possível alcançar vantagem quântica, ou seja, aplicações industriais úteis, com atuais computadores quânticos de escala de ruído intermediária, um conceito disruptivo que chamamos de NISQA. Finalmente, irei oferecer minha perspectiva sobre as excelentes oportunidades para o desenvolvimento de carreiras científicas e empresariais em tecnologias de computação quântica.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2bioygTKTss" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Apresentação: [[pdf](pdfs/Solano2021.pdf)]

-------------------------------

# Quantum Singular Value Transformation: uma estrutura unificada de algoritmos quânticos
*(Palestra em ingles)*

[*András Gylien*](#andrás-gylien)

A computação quântica é poderosa porque os operadores unitários que descrevem a evolução no tempo de um sistema quântico têm tamanho exponencial em termos do número de qubits presentes no sistema. Quantum Singular Value Transformation é uma primitiva algorítmica genérica que permite aproveitar esta vantagem exponencial por meio da execução eficiente de transformações polinomiais para os valores singulares de um bloco de um unitário - generalizando a técnica de Processamento de Sinal Quântico de Low e Chuang. Os circuitos quânticos correspondentes têm uma estrutura muito simples, muitas vezes dão origem a algoritmos ótimos e têm fatores constantes atraentes, enquanto geralmente usam apenas um número constante de qubits auxiliares. Muitos algoritmos quânticos podem ser descritos como um problema de transformação de valor singular, incluindo simulação hamiltoniana, amplificação de amplitude, caminhadas quânticas e algoritmos de álgebra linear quântica para aprendizado de máquina. Como tal, o Quantum Singular Value Transformation leva a uma estrutura unificada de algoritmos quânticos que incorporam uma variedade de vantagens quânticas.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Mu_y5cCum_o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>