---
title: 'Talks'
weight: 30
header_menu: true
---

-------------------------------

# Progress in photonic quantum computation

[*Ernesto Galvão*](#ernesto-galvão)

Recently, there have been impressive demonstrations of small-scale quantum computers in various physical platforms. I will review some of the recent progress in the field, focusing on the use of photonic systems for quantum computation. I will describe boson sampling devices, which are a restricted type of photonic quantum computer, and outline possible paths towards scalable photonic quantum computation.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/9Q9WNnCtud8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Presentation: [[pdf](../pdfs/recent_progress_phot_comp_ufsc_iv_qc_workshop_slides.pdf)]

-------------------------------

# Quantum Computing Market Perspectives and Impact on Global Competitiveness

[*Walmoli Gerber Jr.*](#walmoli-gerber-jr)

The role of the physicist in society goes far beyond the academy. It is a duty of the physicist to promote his/her position in the labor market taking into account the real possibility of undertaking, mainly in the technology sector, once the tools, concepts and reasoning strategies worked out in the physics undergrad course enable technological development.
There are numerous positions where the knowledge on physics has value and great delivery potential. Let's address the quantum computing scenario and demonstrate a Brazilian case of entrepreneurship in physics. QuanBy is the first Brazilian startup focused on the development of digital technologies in quantum computing, with the goal of fostering the development of the quantum computing market in Brazil and offer technologies based on quantum computing and communication.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Qw8dNohcL4k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

-------------------------------

# Co-designing application specific quantum accelerators to HPC

[*Bruno Taketani*](#bruno-taketani)

IQM is focusing on a Co-Design approach where hardware and software are developed hand-in-hand. The resulting application-specific quantum computing stack can be seamlessly integrated into an HPC infrastructure, such that the quantum processor is utilized in an accelerator approach. To achieve this integration IQM is also building HPC-specific software and hardware tools. In this talk we will present these concepts and how collaboration with domain experts will drive us forward. We will also briefly explore possible paths to a career in the growing quantum computing industry.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Y9v96MmwQPk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


-------------------------------

# Quantum Algorithm for Multiobjective Optimization

[*Marcos Villagra*](#marcos-villagra)

In this talk I will discuss about constructions of quantum algorithms for multiobjective optimization. First I will present a heuristic technique based on Grover's search algorithm. Then I will show how to embed a multiobjective problem into a Hamiltonian and use a quantum adiabatic algorithm to find a ground state encoding a Pareto-optimal solution. This talk will be base on these two papers: 1) **Multiobjective Optimization Grover Adaptive Search**. Benjamín Barán & Marcos Villagra. Recent Advances in Computational Optimization. Studies in Computational Intelligence, Vol.795, pp. 191-211, 2019, and 2) **A Quantum Adiabatic Algorithm for Multiobjective Combinatorial Optimization**. Benjamín Barán & Marcos Villagra. Axioms, Vol. 8, No. 1, March 2019.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/AAICWnPN4uo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


-------------------------------

# Quantum algorithms for solving differential equations

[*Ashley Montanaro*](#ashley-montanaro)

Quantum computers are predicted to outperform classical ones for solving differential equations, perhaps exponentially. But will this really be the case? In this talk, I will describe recent work on quantum algorithms for solving differential equations of interest for practical applications. First, we consider a prototypical partial differential equation - the heat equation in a rectangular region - and compare the complexities of eight classical and quantum algorithms for solving it, in the sense of approximately computing the density of heat in a given region. It turns out that, for spatial dimension d <= 3, the quantum algorithms are slower than the best classical ones. For d >= 4, there is a polynomial quantum speedup using an approach based on applying amplitude estimation to a random walk. Second, I will discuss a quantum algorithm for solving differential equations of interest in mathematical finance, such as the Black-Scholes equation, more efficiently than classical methods. This algorithm is based on a quantum speedup of the multilevel Monte Carlo method.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/6NTEpVrVbYw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

-------------------------------

# NISQ Computers for Quantum Advantage (NISQA)

[*Enrique Solano*](#enrique-solano)

I will introduce quantum computing as an emergent field stemming from
quantum physics and quantum information in the frame of theoretical and experimental
physics, and how it has moved towards entrepreneurial worldwide activities. I will explain
how it is possible to reach quantum advantage, meaning useful industrial applications, with
current noisy intermediate-scale quantum computers, a disruptive concept we call NISQA.
I will finally offer my perspective on the outstanding opportunities for developing
scientific and entrepreneurial careers in quantum computing technologies.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2bioygTKTss" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Presentation: [[pdf](../pdfs/Solano2021.pdf)]

-------------------------------

# Quantum Singular Value Transformation: A Unified Framework of Quantum Algorithms

[*András Gylien*](#andrás-gylien)

Quantum computing is powerful because unitary operators
describing the time-evolution of a quantum system have exponential size in
terms of the number of qubits present in the system. Quantum Singular Value
Transformation is a generic algorithmic primitive which enables harnessing
this exponential advantage by efficiently performing polynomial
transformations to the singular values of a block of a unitary --
generalizing the Quantum Signal Processing technique of Low and Chuang. The
corresponding quantum circuits have a very simple structure, often give
rise to optimal algorithms and have appealing constant factors, while
usually only use a constant number of ancilla qubits. Many quantum
algorithm can be cast as a singular value transformation problem, including
Hamiltonian simulation, amplitude amplification, quantum walks and quantum
linear algebra algorithms for machine learning. As such, Quantum Singular
Value Transformation leads to a unified framework of quantum algorithms
incorporating a variety of quantum speed-ups.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Mu_y5cCum_o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>