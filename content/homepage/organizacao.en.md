---
title: 'Organization'
weight: 65
---

* Prof. Eduardo Inacio Duzzioni, Phd. - Coordinator
* Prof. Jerusa Marchi, Phd. - Coordinator				
* Prof. Gisele Bosso de Freitas, Phd.				
* Prof. Paulo Mafra, Phd.				
* Prof. Rafael de Santiago, Phd.
* Prof. Clovis Aparecido Caface Filho, Me.				
* Caio Boccato Dias de Góes, Me.
* Evandro Chagas Ribeiro da Rosa, Me.			
* Otto Menegasso Pires				
* Eduardo Willwock Lussi
* Daniel Boso
* Gabriel Medeiros Lopes Carneiro
* Gilson Trombeta Magro				
* Tomaz Souza Cruz	
