---
title: 'Hackathon'
weight: 21
header_menu: true
---

Have you ever compose your team? The quantum computing hackathon is coming!

The teams will be composed of 3 members who will face 3 challenges with increasing degrees of difficulty, putting to the test what you learned during the event's mini-courses!

It will be 24 hours of pure adrenaline! The challenges will be announced at 11am on 10/29 and teams will have until 11am on 10/30 to submit their solutions!

The promotion and awarding of the first three best placed teams will take place during the closing of the event at 12:00 on 10/30.

Further details about the event's dynamics will be sent by email. [Sign up!](https://forms.gle/MWDuiu3pw248mxRH9)

Hackathon Rules (PT-BR) [[pdf](../pdfs/REGULAMENTO_HACKATHON.pdf)]