---
title: 'Programação'
weight: 20
header_menu: true
---

<div style="overflow: auto;">
    <table class="table programacao" style="color: black; margin-bottom: 0px;">
        <thead class="">
            <tr>
                <th class="tamanho-coluna-horas"></th>
                <th class="tamanho-coluna-semana">Segunda-feira (25/10)</th>
                <th class="tamanho-coluna-semana">Terça-feira (26/10)</th>
                <th class="tamanho-coluna-semana">Quarta-feira (27/10)</th>
                <th class="tamanho-coluna-semana">Quinta-feira (28/10)</th>
                <th class="tamanho-coluna-semana">Sexta-feira (29/10)</th>
                <th class="tamanho-coluna-semana">Sábado (30/10)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th >8h</th>
                <td></td>
                <td>
                    <a href="#computadores-nisq-para-quantum-advantage-nisqa">Computadores NISQ para Quantum Advantage (NISQA)</a>
                </td>
                <td></td>
                <td>
                    <a href="#o-que-há-de-errado-com-os-computadores-quânticos-atuais-e-como-podemos-melhorá-los">O que há de errado com os computadores quânticos atuais e como podemos melhorá-los?</a>
                </td>
                <td>
                    <a href="#o-que-há-de-errado-com-os-computadores-quânticos-atuais-e-como-podemos-melhorá-los">O que há de errado com os computadores quânticos atuais e como podemos melhorá-los?</a>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>9h</th>
                <td rowspan="2" style="vertical-align: middle;">
                    <a href="#introdução-a-computação-quântica-com-ket">Introdução à Computação Quântica com Ket</a>
                </td>
                <td>
                    <a href="#algoritmos-quânticos-para-resolver-equações-diferenciais">Algoritmos quânticos para resolver equações diferenciais</a>
                </td>
                <td></td>
                <td>
                    <a href="#progressos-em-computação-quântica-fotônica">Progressos em computação quântica fotônica</a>
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>10h</th>
                <td>
                    <a href="#quantum-singular-value-transformation-uma-estrutura-unificada-de-algoritmos-quânticos">Quantum Singular Value Transformation: uma estrutura unificada de algoritmos quânticos</a>
                </td>
                <td>
                    <a href="#perspectivas-do-mercado-da-computação-quântica-e-impacto-na-competitividade-global">Perspectivas do mercado da computação quântica e impacto na competitividade global</a>
                </td>
                <td>
                    <a href="#co-projetando-aceleradores-quânticos-específicos-de-aplicativos-para-hpc">Co-projetando aceleradores quânticos específicos de aplicativos para HPC</a>
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>11h</th>
                <td>
                    <a href="#algoritmo-quântico-para-otimização-multiobjetivo">Algoritmo Quântico para Otimização Multiobjetivo</a>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td>Hackathon Início</td>
                <td>Hackathon Fim</td>
            </tr>
            <tr>
                <th>12h</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>13h</th>
                <td rowspan="3"></td>
                <td rowspan="3"></td>
                <td rowspan="3"></td>
                <td rowspan="3"></td>
                <td rowspan="3"></td>
                <td rowspan="3"></td>
            </tr>
            <tr>
                <th>14h</th>
            </tr>
            <tr>
                <th>15h</th>
            </tr>
            <tr>
                <th>16h</th>
                <td rowspan="2">
                    <a href="#introdução-a-computação-quântica-com-ket">Introdução à Computação Quântica com Ket</a>
                </td>
                <td rowspan="2">
                    <a href="#introdução-à-informação-quântica-usando-o-qiskit">Introdução à Informação Quântica usando o Qiskit</a>
                </td><td>
                </td>
                <td rowspan="2">
                    <a href="#introdução-à-informação-quântica-usando-o-qiskit">Introdução à Informação Quântica usando o Qiskit</a>
                </td>
                <td></td>
                <td>Resultados do Hackathon e
                Encerramento do Workshop</td>
            </tr>
            <tr>
                <th>17h</th>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

<style>

    .tamanho-coluna-horas {
        min-width: 35px;
    }

    .tamanho-coluna-semana {
        min-width: 180px;
    }

    .programacao > thead > tr > th, .programacao > tbody > tr > th {
        background-color: #26a69a !important;
        border: 0 !important;
    }

    td {
        border: 0 !important;
        vertical-align: middle !important;
        word-wrap: normal !important;
    }

    td > a {
        text-decoration: none;
    }

    td > a:hover {
        color: black;
        font-weight: bold;
    }

    table tbody > tr:nth-child(odd) > td, table tbody > tr:nth-child(odd) > th {
        background-color: white;
    }

    table tbody > tr:nth-child(even) > td, table tbody > tr:nth-child(even) > th {
        background-color: #ddf2f0;
    }

</style>

# Palestras

Segunda-feira (25/10)

* 11h - [Algoritmo Quântico para Otimização Multiobjetivo](#algoritmo-quântico-para-otimização-multiobjetivo)

Terça-feira (26/10)

* 8h - [Computadores NISQ para Quantum Advantage (NISQA)](#computadores-nisq-para-quantum-advantage-nisqa)
* 9h - [Algoritmos quânticos para resolver equações diferenciais](#algoritmos-quânticos-para-resolver-equações-diferenciais)
* 10h - [Quantum Singular Value Transformation: uma estrutura unificada de algoritmos quânticos](#quantum-singular-value-transformation-uma-estrutura-unificada-de-algoritmos-quânticos)

Quarta-feira (27/10)

* 10h - [Perspectivas do mercado da computação quântica e impacto na competitividade global](#perspectivas-do-mercado-da-computação-quântica-e-impacto-na-competitividade-global)

Quinta-feira (28/10)

* 9h - [Progressos em computação quântica fotônica](#progressos-em-computação-quântica-fotônica)
* 10h - [Co-projetando aceleradores quânticos específicos de aplicativos para HPC](#co-projetando-aceleradores-quânticos-específicos-de-aplicativos-para-hpc)

# Minicursos

[**Introdução a Computação Quântica com Ket**](#introdução-a-computação-quântica-com-ket)

Minicurso de 4h. 
 * Segunda-feira (25/10) das 9h as 11h 
 * Segunda-feira (25/10) das 16h as 18h

[**O que há de errado com os computadores quânticos atuais e como podemos melhorá-los?**](#o-que-há-de-errado-com-os-computadores-quânticos-atuais-e-como-podemos-melhorá-los)

Minicurso de 2h. 
* Quinta-feira (28/10) das 8h as 9h 
* Sexta-feira (29/10) das 8h as 9h

[**Introdução à informação quântica usando o Qiskit**](#introdução-à-informação-quântica-usando-o-qiskit)

Minicurso de 4h. 
 * Terça-feira (26/10) das 16h as 18h 
 * Quinta-feira (28/10) das 16h as 18h