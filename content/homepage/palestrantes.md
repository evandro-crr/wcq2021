---
title: 'Palestrantes'
weight: 50
header_menu: false
---

--------------------------------------

# András Gylien
*Caltech (EUA)*

![András Gylien](fotos/andras.jpg)

András Gilyén é bolsista Marie Curie do Instituto Rényi. Seus principais tópicos de pesquisa são algoritmos quânticos e complexidade, com foco particular em métodos de álgebra linear quântica (transformação de valor singular quântico e a estrutura de codificação de bloco), otimização e caminhadas quânticas. András recebeu seu PhD em 2019 pela University of Amsterdam, onde foi orientado por Ronald de Wolf e co-orientado por Harry Buhrman no CWI/QuSoft. Entre 2019 e 2021 ele foi um pós-doutorado IQIM na Caltech, enquanto isso ele recebeu o prêmio ERCIM Cor Baayen Young Researcher em 2019 e foi um pesquisador do Google no Instituto Simons para a Teoria da Computação em Berkeley durante o programa "The Quantum Wave in Computing" na primavera de 2020.

--------------------------------------

# André Carvalho
*Q-CTRL (Austrália)*

![André Carvalho](fotos/andre.jpg)

Andre Carvalho é o Quantum Professional Services Lead na Q-CTRL. Ele completou seu PhD na Universidade Federal do Rio de Janeiro antes de ingressar no Instituto Max-Planck de Física de Sistemas Complexos em Dresden como um pós-doutorado. Ele então passou mais de uma década como pesquisador na Australian National University, onde trabalhou em várias disciplinas, reunindo a experiência de físicos e engenheiros para desenvolver soluções de controle para problemas de tecnologia quântica. Ele também trabalhou como pesquisador sênior na Griffith University. Andre tem uma vasta experiência internacional e é amplamente reconhecido por suas contribuições ao campo do controle quântico.

--------------------------------------

# Ashley Montanaro
*University of Bristol (Reino Unido)*

![Ashley Montanaro](fotos/montanaro.jpg)

Ashley Montanaro trabalha na área de computação quântica há 17 anos. Sua especialidade é algoritmos quânticos e complexidade computacional quântica, temas sobre os quais publicou mais de 50 artigos. Ele tem PhD em computação quântica pela University of Bristol, sob orientação do Prof. Richard Jozsa. Fez pós-doutorado na University of Cambridge e atualmente é professor de Computação Quântica em Bristol. Já foi bolsista ERC Consolidator e premiado com o Prêmio Whitehead em 2017 pela London Mathematical Society. Fez parte do Comitê Diretor da conferência internacional sobre Processamento de Informação Quântica (QIP) de 2016-19, foi Editor Fundador da revista Quantum. Também é cofundador da Phasecraft, uma startup de software quântico cujo objetivo é obter o máximo desempenho dos computadores quânticos.

--------------------------------------

# Bruna Gabrielly de Moraes Araújo
*The Institute of Photonic Sciences (Espanha)*

![Bruna Gabrielly de Moraes Araújo](fotos/bruna.jpg)

Bacharelado em Eng. Química pela Unicap - PE. Bacharelado em Física Teórica na UFPE (Recife, Brasil) e na Universidade do Porto (Porto, Portugal).
Mestrado em Física onde trabalhei com teoria lógica quântica em uma armadilha iônica de um único íon. Doutoranda em física no ICFO,ICN2 &UAB. Tópico de tese: Entanglement in many-body physics. Fiz um intercâmbio na IBM Quantum de Abril até Setembro.

--------------------------------------

# Bruno Taketani
*IQM Quantum Computers (Alemanha)*

![Bruno Taketani](fotos/bruno.png)

Bruno Taketani fez seu doutorado na Universidade Federal do Rio de Janeiro, trabalhando com óptica quântica e protocolos de computação/informação quântica. Fez dois pós-doutorados na University of Saarland (Alemanha), desenvolvendo protocolos e algoritmos para computação quântica com íons e qubits supercondutores. Desde 2016 é professor na Universidade Federal de Santa Catarina, onde ajudou a fortalecer as pesquisas em computação quântica. Em setembro de 2020 passou a liderar a equipe de algoritmos e aplicativos, bem como as pesquisas para integrar os computadores quânticos da IQM em ambientes HPC, na IQM Germany.

--------------------------------------

# Enrique Solano
*Kipu Quantum (Alemanha) \
University of the Basque Country (Espanha) \
Shanghai University (China)*

![Enrique Solano](fotos/enrique.jpg)

O Prof. Enrique Solano obteve seu Bacharelado e Mestrado em Física pela Pontificia Universidad Católica del Perú, Lima, Peru. Obteve seu doutorado em Física na Universidade Federal do Rio de Janeiro, Rio de Janeiro, Brasil. Realizou pós-doutorado no Instituto Max-Planck de Óptica Quântica, Garching, Alemanha e no Ludwig-Maximilian University, Munich, Germany. Em 2000, foi nomeado professor de Física na Pontificia Universidad Católica del Perú, Lima, Peru. Em 2008, ele aceitou a posição de prestígio de Professor Ikerbasque na Universidade do País Basco, Bilbao, Espanha. Foi diretor do centro internacional Quantum Computing and Quantum Technologies (QUTIS). Em 2018, aceitou o cargo de Distinto Professor de Física em Xangai Universidade, Xangai, China, onde foi diretor do centro internacional "Quantum Artificial Intelligence for Science and Technology (QuArtist)". Em 2019, foi nomeado Diretor Científico da empresa de IA DeepBlue Technology com base em Xangai, China. Em 2020, o Prof. Solano ocupou o cargo de CEO da IQM Germany. Desde 2021, é CEO da Kipu Quantum, impulsionando computadores quânticos de co-design modulares de multiplataforma.

--------------------------------------

# Ernesto Galvão
*International Iberian Nanotechnology Laboratory (Portugal) \
Universidade Federal Fluminense (Brasil)*

![Ernesto Galvão](fotos/ernesto.jpg)

Ernesto F. Galvão é o líder do grupo Quantum and Linear-Optical Computation do INL. Doutor em Física pela Universidade de Oxford (PhD 2002), mestre pela Universidade Federal do Rio de Janeiro (1998) e bacharel pela Pontifícia Universidade Católica do Rio de Janeiro (1996). Foi pós-doutorando no Perimeter Institute for Theoretical Physics (Canadá) entre 2002 e 2005 e atualmente está de licença do Instituto de Física da Universidade Federal Fluminense (Brasil), onde é docente desde 2006. Em sua pesquisa, ele estuda diferentes modelos computacionais quânticos para identificar e quantificar recursos capazes de alcançar uma vantagem quântica no processamento da informação. Ele está particularmente interessado em implementações fotônicas de computadores quânticos e colabora com grupos de óptica quântica experimental para implementar e caracterizar esses dispositivos.

--------------------------------------

# Evandro Chagas
*Universidade Federal de Santa Catarina (Brasil)*

![Evandro Chagas](fotos/evandro.jpg)

Evandro é mestrando do Programa de Pós-Graduação em Ciência da computação da Universidade Federal de Santa Catarina, e é graduado em Ciências da Computação pela mesma universidade. No seu mestrado, ele desenvolveu a linguagem de programação clássica-quântica Ket, que tem como foco a computação quântica em nuvem e a interação entre informações clássicas e quânticas. Sua área de pesquisa também abrange a simulação de computação quântica, onde ele desenvolveu novas técnicas que permitem diminuir o tempo de simulação de certos algoritmos quânticos. Evandro é membro do Grupo de Computação Quântica da UFSC, onde participou da organização de todas as edições do Workshop de Computação Quântica.

--------------------------------------


# Marcos Villagra
*Universidad Nacional de Asunción (Paraguai)*

![Marcos Villagra](fotos/marcos.png)

Marcos Villaga recebeu o título de PhD em Ciência da Computação Teórica pelo Nara Institute of Science and Technology, Japão em março de 2013. Foi Pesquisador da Sociedade Japonesa para a Promoção das Ciências (JSPS) e pesquisador de pós-doutorado na Universidade de Fukui de abril de 2013 a março de 2015. Desde abril de 2015, é professor pesquisador em tempo integral na Universidade Nacional de Assunção, Paraguai. Seus interesses de pesquisa são: complexidade computacional e algébrica, criptografia, computação e informação quântica, jogos combinatórios, teoria de autômatos e algoritmos algébricos e espectrais.

--------------------------------------

# Walmoli Gerber Jr.
*QuanBy Computação Quântica (Brasil)*

![Walmoli Gerber Jr.](fotos/walmoli.jpg)

Bacharel em Física pela UFSC, especialista em física médica,  Diretor de novos negócios da Quanby Computação Quântica, Diretor Executivo da Brasilrad Física Médica, Diretor da Vertical Saúde da ACATE, Investidor Anjo e conselheiro de inovação.