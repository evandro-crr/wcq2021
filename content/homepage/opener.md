---
title: 'Sobre'
weight: 10
---

Transmissão de encerramento:
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2GY7hBiU7a4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Mais uma vez online, o IV Workshop de Computação Quântica reúne nomes da academia e indústria com o objetivo de atualizar a comunidade brasileira sobre os rápidos avanços da Computação Quântica. Tal revolução baseia-se no uso da mecânica quântica para implementar uma nova lógica de processamento e armazenamento de informação. Embora em estágio embrionário, os processadores quânticos [Sycamore](https://www.nature.com/articles/s41586-019-1666-5) da Google, [Jiuzhang](https://science.sciencemag.org/content/370/6523/1460.abstract) e [Zuchongzhi (arXiv)](https://arxiv.org/abs/2106.14734), ambos da China, já superam o poder de processamento de supercomputadores como Summit e Sunway TaihuLight. Em alguns casos os computadores quânticos são cerca de 10 milhões de bilhões de vezes mais rápidos do que a sua simulação clássica! 

O [Grupo de Computação Quântica da UFSC](http://www.gcq.ufsc.br/) convida você a participar desta revolução científica e tecnológica! Apresentaremos os recentes avanços na área através de palestras e minicursos. Você ainda terá a oportunidade de participar de um hackathon quântico. 
Abaixo temos mais informações.

### [Inscreva-se!](#inscrição) (Gratuito)

Mais informações sobre as plataformas usadas para acompanhar o workshop
serão divulgadas em breve. Não deixe de se inscrever.

### Como participar

Para participar do evento basta se [inscrever](#inscrição) e acompanhar as palestras ao vivo pelo YouTube. Durante as apresentações será possível fazer perguntas (inclusive de maneira anônima) e votar em perguntas de outras pessoas utilizando a plataforma Slido. Ao final de cada sessão, as perguntas mais votadas serão feitas ao palestrante. 
