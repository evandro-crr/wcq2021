---
title: 'Schedule'
weight: 20
header_menu: true
---

<div style="overflow: auto;">
    <table class="table programacao" style="color: black; margin-bottom: 0px;">
        <thead class="">
            <tr>
                <th class="tamanho-coluna-horas"></th>
                <th class="tamanho-coluna-semana">Monday (25/10)</th>
                <th class="tamanho-coluna-semana">Tuesday (26/10)</th>
                <th class="tamanho-coluna-semana">Wednesday (27/10)</th>
                <th class="tamanho-coluna-semana">Thursday (28/10)</th>
                <th class="tamanho-coluna-semana">Friday (29/10)</th>
                <th class="tamanho-coluna-semana">Saturday (30/10)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th >8h</th>
                <td></td>
                <td>
                    <a href="#nisq-computers-for-quantum-advantage-nisqa">NISQ Computers for Quantum Advantage (NISQA)</a>
                </td>
                <td></td>
                <td>
                    <a href="#what-is-wrong-with-current-quantum-computers-and-how-can-we-improve-them">What is wrong with current quantum computers and how can we improve them?</a>
                </td>
                <td>
                    <a href="#what-is-wrong-with-current-quantum-computers-and-how-can-we-improve-them">What is wrong with current quantum computers and how can we improve them?</a>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>9h</th>
                <td rowspan="2" style="vertical-align: middle;">
                    <a href="#quantum-computing-introduction-with-ket">Quantum Computing Introduction with Ket</a>
                </td>
                <td>
                    <a href="#quantum-algorithms-for-solving-differential-equations">Quantum algorithms for solving differential equations</a>
                </td>
                <td></td>
                <td>
                    <a href="#progress-in-photonic-quantum-computation">Progress in photonic quantum computation</a>
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>10h</th>
                <td>
                    <a href="#quantum-singular-value-transformation-a-unified-framework-of-quantum-algorithms">Quantum Singular Value Transformation: A Unified Framework of Quantum Algorithms</a>
                </td>
                <td>
                    <a href="#quantum-computing-market-perspectives-and-impact-on-global-competitiveness">Quantum Computing Market Perspectives and Impact on Global Competitiveness</a>
                </td>
                <td>
                    <a href="#co-designing-application-specific-quantum-accelerators-to-hpc">Co-designing application specific quantum accelerators to HPC</a>
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>11h</th>
                <td>
                    <a href="#quantum-algorithm-for-multiobjective-optimization">Quantum Algorithm for Multiobjective Optimization</a>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td>Start of Hackathon</td>
                <td>End of Hackathon</td>
            </tr>
            <tr>
                <th>12h</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>13h</th>
                <td rowspan="3"></td>
                <td rowspan="3"></td>
                <td rowspan="3"></td>
                <td rowspan="3"></td>
                <td rowspan="3"></td>
                <td rowspan="3"></td>
            </tr>
            <tr>
                <th>14h</th>
            </tr>
            <tr>
                <th>15h</th>
            </tr>
            <tr>
                <th>16h</th>
                <td rowspan="2">
                    <a href="#quantum-computing-introduction-with-ket">Quantum Computing Introduction with Ket</a>
                </td>
                <td rowspan="2">
                    <a href="#introduction-to-quantum-information-using-qiskit">Introduction to quantum information using Qiskit</a>
                </td>
                <td></td>
                <td rowspan="2">
                    <a href="#introduction-to-quantum-information-using-qiskit">Introduction to quantum information using Qiskit</a>
                </td>
                <td></td>
                <td>Hackathon Results and 
                Closing of the Workshop</td>
            </tr>
            <tr>
                <th>17h</th>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

<style>

    .tamanho-coluna-horas {
        min-width: 35px;
    }

    .tamanho-coluna-semana {
        min-width: 180px;
    }

    .programacao > thead > tr > th, .programacao > tbody > tr > th {
        background-color: #26a69a !important;
        border: 0 !important;
    }

    td {
        border: 0 !important;
        vertical-align: middle !important;
        word-wrap: normal !important;
    }

    td > a {
        text-decoration: none;
    }

    td > a:hover {
        color: black;
        font-weight: bold;
    }

    table tbody > tr:nth-child(odd) > td, table tbody > tr:nth-child(odd) > th {
        background-color: white;
    }

    table tbody > tr:nth-child(even) > td, table tbody > tr:nth-child(even) > th {
        background-color: #ddf2f0;
    }

</style>

# Talks 

Monday (25/10)

* 11h - [Quantum Algorithm for Multiobjective Optimization](#quantum-algorithm-for-multiobjective-optimization)

Tuesday (26/10)

* 8h - [NISQ Computers for Quantum Advantage (NISQA)](#nisq-computers-for-quantum-advantage-nisqa)
* 9h - [Quantum algorithms for solving differential equations](#quantum-algorithms-for-solving-differential-equations)
* 10h - [Quantum Singular Value Transformation: A Unified Framework of Quantum Algorithms](#quantum-singular-value-transformation-a-unified-framework-of-quantum-algorithms)

Wednesday (27/10)

* 10h - [Quantum Computing Market Perspectives and Impact on Global Competitiveness](#quantum-computing-market-perspectives-and-impact-on-global-competitiveness)

Thursday (28/10)

* 9h - [Progress in photonic quantum computation](#progress-in-photonic-quantum-computation)
* 10h - [Co-designing application specific quantum accelerators to HPC](#co-designing-application-specific-quantum-accelerators-to-hpc)

# Minicourses

[**Quantum Computing Introduction with Ket**](#quantum-computing-introduction-with-ket)

4h. duration. 
 * Monday (25/10) from 9h to 11h 
 * Monday (25/10) from 16h to 18h

[**What is wrong with current quantum computers and how can we improve them?**](#what-is-wrong-with-current-quantum-computers-and-how-can-we-improve-them)

2h duration. 
* Thursday (28/10) from 8h to 9h 
* Friday (29/10) from 8h to 9h

[**Introduction to quantum information using Qiskit**](#introduction-to-quantum-information-using-qiskit)

4h duration.
* Tuesday (26/10) from 4 pm to 6 pm
* Thursday (28/10) from 4 pm to 6 pm 

