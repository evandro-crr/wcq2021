---
title: 'About'
weight: 10
---

Closing Transmission:
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2GY7hBiU7a4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Once again online, the IV Quantum Computing Workshop brings together 
names from academia and industry in order to update the Brazilian 
community on the rapid advances in Quantum Computing. This revolution 
is based on the use of quantum mechanics to implement a new information 
processing and storage logic. Although at an embryonic stage, the 
quantum processors [Sycamore](https://www.nature.com/articles/s41586-019-1666-5) from Google, [Jiuzhang](https://science.sciencemag.org/content/370/6523/1460.abstract) and [Zuchongzhi (arXiv)](https://arxiv.org/abs/2106.14734), both 
from China, already surpass the processing power of supercomputers 
like Summit and Sunway TaihuLight. In some cases quantum computers are 
about 10 million billion times faster than your classic simulation!

The [UFSC Quantum Computing Group](http://www.gcq.ufsc.br) invites you to participate in this 
scientific and technological revolution! We will present recent advances in the area 
through live lectures and minicourses. You will even have the opportunity to 
participate in a quantum hackathon. Below we have more information. 

### [Register!](#registration)

More information about the used platforms used to follow the workshop 
will be released soon. Be sure to make a registration. 

### How to participate

To participate in the event, just [register](#registration) and follow the 
talks live on YouTube (links in [program](#program)). During the presentations 
it will be possible to ask questions (anonymously if you prefer) and vote on 
other people's questions using the Slido platform. 
At the end of each session, the most voted questions will be asked to the speaker. 

