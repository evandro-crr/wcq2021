---
title: 'Hackathon'
weight: 21
header_menu: true
---

Você já montou a sua equipe? O hackathon de computação quântica está chegando!

As equipes serão compostas por 3 membros que enfrentarão 3 desafios com graus de dificuldade crescente, pondo a prova o que você aprendeu durante os minicursos do evento!

Serão 24 horas de pura adrenalina! Os desafios serão divulgados as 11h do dia 29/10  e as equipes terão até as  11h do dia 30/10 para submeterem suas soluções!

A divulgação e a premiação das três primeiras equipes melhor colocadas acontecerá durante o encerramento do evento as 12h do dia 30/10.

Maiores detalhes sobre a dinâmica do evento serão enviados por e-mail. [Inscreva-se!](https://forms.gle/MWDuiu3pw248mxRH9)

Regulamento do Hackathon [[pdf](pdfs/REGULAMENTO_HACKATHON.pdf)]
