---
title: 'Organização'
weight: 70
---

* Prof. Eduardo Inacio Duzzioni, Dr. - Coordenador		
* Profª Jerusa Marchi, Drª. - Coordenadora				
* Profª Gisele Bosso de Freitas, Drª.				
* Prof. Paulo Mafra, Dr.				
* Prof. Rafael de Santiago, Dr.
* Prof. Clovis Aparecido Caface Filho, Me.				
* Caio Boccato Dias de Góes, Me.
* Evandro Chagas Ribeiro da Rosa, Me.				
* Otto Menegasso Pires				
* Eduardo Willwock Lussi
* Daniel Boso
* Gabriel Medeiros Lopes Carneiro
* Gilson Trombeta Magro				
* Tomaz Souza Cruz	