---
title: 'Minicursos'
weight: 40
header_menu: true
---

-------------------------------

# Introdução a Computação Quântica com Ket

[*Evandro Chagas*](#evandro-chagas)

Neste minicurso vamos introduzir do zero conceitos básicos de computação quântica e testá-los na linguagem de programação quântica [Ket](https://quantum-ket.gitlab.io). Durante o minicurso, vamos usar o simulador de computação quântica KBW para olhar dentro da execução quântica, algo que não é possível utilizando um computador quântico. Esperamos que todos os participantes interajam durante o curso, manifestando suas dúvidas e testando o que aprenderam. Para acompanhar o minicurso, não é necessário ter conhecimento prévio em computação quântica. Porém, noções básicas de álgebra linear e programação em Python irão ajudar a acompanhar o minicurso.

<details>
  <summary><b>» Requisitos necessários para acompanhar o minicurso</b></summary>
  
* Sistema operacional Linux atualizado 
* Python 3.7 ou superior
* [Ket](https://gitlab.com/quantum-ket/ket)
* [KBW](https://gitlab.com/quantum-ket/kbw)

Para instalar o Ket e o KBW basta executar:
```console
pip install ket-lang kbw
```
</details>

<details>
  <summary><b>» Se você não possui Linux e/ou não é familiarizado com o sistema</b></summary>
  
Recomendamos criar uma máquina virtual com [Ubuntu 20.04](https://ubuntu.com/download/desktop). Após instalado o sistema operacional, para instalar as ferramentas necessárias, abra um terminal e cole linha a linha os seguintes comandos:

```console
sudo apt update -y
sudo apt install python3-pip -y 
pip3 install ket-lang kbw
```

Para abrir o servidor do KBW basta executar: 
```console
python3 -m kbw
```

Para a programação quântica durante o minicurso, recomendamos usar o Visual Studio Code. Para instalar no Ubuntu basta executar o seguinte comando no terminal:
```console
sudo snap install code --classic
```
</details>

Apresentação: [[pdf](pdfs/ket.pdf)] \
QAOA: [[zip](pdfs/qaoa_ket.zip)]

Parte 1:
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/LnNh-l1_bNc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Parte 2:
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/E44ejmGi7lg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

-------------------------------

# Introdução à informação quântica usando o Qiskit 

[*Bruna Gabrielly de Moraes Araújo*](#bruna-gabrielly-de-moraes-araújo)

Vamos ter uma básica introdução sobre informação quântica, computação quântica até o protocolo de teletransporte e depois vamos aplicar estas ideias usando as ferramentas do Qiskit e executando alguns experimentos usando um computador quântico de verdade!

Pré-requisitos, recomendações e ferramentas necessárias para realizar o minicurso.

Qualquer pessoa que não sabe mecânica quântica ou mesmo programação pode ser capaz de seguir o curso.
 
Recomendo que todos façam um conta aqui: https://quantum-computing.ibm.com

Notebooks: [[zip](pdfs/notebooks_qiskit.zip)]

Parte 1:
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/udNkZowXQ9E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Parte 2:
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/oMKxONptg2A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

-------------------------------

# O que há de errado com os computadores quânticos atuais e como podemos melhorá-los?

[*André Carvalho*](#andré-carvalho)

Neste breve curso, discutiremos as limitações atuais dos computadores quânticos, os desafios enfrentados ao executar algoritmos em dispositivos reais e como as técnicas de controle quântico podem ser usadas para melhorar o desempenho do hardware quântico. Apresentaremos Boulder Opal, o produto da Q-CTRL que visa caracterizar, melhorar e automatizar o hardware quântico. Começando com um modelo idealizado de hardware para qubit supercondutor, você aprenderá como adicionar diferentes camadas de imperfeições até alcançar um modelo razoavelmente realista do sistema quântico. Você poderá usar este modelo para simular a aplicação de portas quânticas no qubit e analisar, com a ajuda das ferramentas de visualização do Boulder Opal, os efeitos das imperfeições no desempenho final da porta quântica. Em seguida, apresentaremos diferentes estratégias de controle quântico para mitigar erros no sistema, suas vantagens e desvantagens; e aplicamos elas ao  modelo de qubit realista apresentado anteriormente.

Parte 1:
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2dJ31N7gq48" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Parte 2:
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/3fOnBJcXUTQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
